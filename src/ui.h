#include <stdbool.h>

#ifndef UI_H
#define UI_H

bool inicializar_ui(void);
void finalizar_ui(void);
void inicializar_loop_principal(void);

#endif
