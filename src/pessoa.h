#include <stdbool.h>

#ifndef PESSOA_H
#define PESSOA_H

#define TAMANHO_NOME 151
#define TAMANHO_CPF 13
#define TAMANHO_TEL 13

struct Pessoa
{
	char nome[TAMANHO_NOME]; 
	char cpf[TAMANHO_CPF];
	char telefone[TAMANHO_TEL]; 
	unsigned char idade; 
	char sexo;
};

//compara pessoas pelo cpf
bool compara_pessoas(struct Pessoa* p1, struct Pessoa* p2);

#endif
