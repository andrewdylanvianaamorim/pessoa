#include <stdbool.h>
#include <stdlib.h>
#include "pessoa.h"
#ifndef BACKEND_H
#define BACKEND_H

enum FLGAS 
{
    NAO_ALTERA_NADA = 0,
    MUDAR_NOME = 1 << 0,
    MUDAR_CPF = 1 << 1,
    MUDAR_TELEFONE = 1 << 2,
    MUDAR_IDADE = 1 << 3,
    MUDAR_SEXO = 1 << 4
};

enum RETORNO_ADICIONAR_PESSOA
{
    SUCESSO,
    PESSOA_JA_CADASTRADA,
    OUTRO_ERRO,   
};

bool inicializar_backend(void);
void desligar_backend(void);
// temos que garantir a integridade do cpf
int adicionar_pessoas(struct Pessoa pessoas[], size_t quantidade);
bool remover_pessoa(const char *cpf);
// temos que garantir a integridade  do cpf
bool atualizar_pessoa(char *cpf, const struct Pessoa *nova_pessoa, unsigned char flags);

size_t pega_quantidade_elementos(void);

// o fim está incluso
void iterar(size_t comeco, size_t fim, void (func)(struct Pessoa *atual, size_t index, void *dado) , void *dado);

#endif