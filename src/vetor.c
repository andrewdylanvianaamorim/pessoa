#include "vetor.h"
#include "pessoa.h"
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#define FATOR_CRESCIMENTO 2
#define TAMANHO_INICIAL 8

struct Vetor *criar_vetor(void) 
{
	struct Pessoa* buffer_pessoa = calloc(TAMANHO_INICIAL, sizeof(struct Pessoa));
	if (buffer_pessoa == NULL) return NULL;
	struct Vetor* vetor = calloc(sizeof(struct Pessoa), 1);
	if (buffer_pessoa == NULL) return NULL;
	vetor->buffer_pessoa = buffer_pessoa;
	vetor->quantidade_elementos = 0;
	vetor->capacidade = TAMANHO_INICIAL;
	return vetor;
}

void deletar_vetor(struct Vetor** vetor)
{
	if (vetor == NULL) return;
	if ((*vetor)->buffer_pessoa != NULL) free((*vetor)->buffer_pessoa);
	free(*vetor);
	*vetor = NULL;
}

// só retorna falso se houver algum erro
bool crescer_vetor(struct Vetor *vetor)
{
	// -2 para ter um de reserva hehe
	if (vetor->quantidade_elementos > vetor->capacidade - 2)
	{
		struct Pessoa* novo_pessoa_buffer = realloc(vetor->buffer_pessoa, sizeof(struct Pessoa) * vetor->capacidade * FATOR_CRESCIMENTO);
		if (novo_pessoa_buffer == NULL) return false;
		vetor->buffer_pessoa = novo_pessoa_buffer;
		vetor->capacidade *= FATOR_CRESCIMENTO;
	}
	return true;
}

bool adicionar_elemento(struct Vetor *vetor,struct Pessoa* pessoa) {
	if (!crescer_vetor(vetor)) return false;
	memcpy(&vetor->buffer_pessoa[vetor->quantidade_elementos], pessoa, sizeof(struct Pessoa));
	vetor->quantidade_elementos++;
	return true;
}

struct Pessoa* pegar_elemento_indice_ref(struct Vetor* vetor, size_t indice)
{
	if (indice < vetor->quantidade_elementos)
		return &vetor->buffer_pessoa[indice];
	return NULL;
}

bool  pegar_elemento_indice(struct Vetor *vetor, size_t indice, struct Pessoa *elemento)
{
	struct Pessoa *pessoa = pegar_elemento_indice_ref(vetor, indice);
	if (pessoa == NULL || elemento == NULL) return false;
	*elemento = *pessoa;
	return true;
}

size_t pegar_indice(struct Vetor* vetor, struct Pessoa* pessoa, bool* deu_certo)
{
	size_t index = 0;
	for (; index < vetor->quantidade_elementos; index++)
		if (compara_pessoas(&vetor->buffer_pessoa[index], pessoa)) break;

	if (!(*deu_certo = index < vetor->quantidade_elementos))
		index = 0;

	return index;
		
}

bool deletar_indice(struct Vetor* vetor, size_t indice, struct Pessoa* excluido)
{
	struct Pessoa *antigo = pegar_elemento_indice_ref(vetor, indice);
	
	if (antigo == NULL) return false;



	if (excluido != NULL) memcpy(excluido, antigo, sizeof(struct Pessoa));


	if (indice < vetor->quantidade_elementos - 1) {
		memcpy(antigo, antigo + 1, sizeof(struct Pessoa) * (vetor->quantidade_elementos - indice + 1));
	}

	vetor->quantidade_elementos--;

	return true;
}
