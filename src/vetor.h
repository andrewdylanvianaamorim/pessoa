#include "pessoa.h"
#include <stdbool.h>
#include <stdlib.h>

#ifndef VETOR_H
#define VETOR_H

struct Vetor
{
	struct Pessoa* buffer_pessoa;
	size_t quantidade_elementos;
	size_t capacidade;
};

struct Vetor *criar_vetor(void);
void deletar_vetor(struct Vetor **vetor);
bool adicionar_elemento(struct Vetor *vetor,struct Pessoa* pessoa);
struct Pessoa* pegar_elemento_indice_ref(struct Vetor* vetor, size_t indice); //CUIDADO COM A VIDA DAS REFERÊNCIAS
bool pegar_elemento_indice(struct Vetor* vetor, size_t indice, struct Pessoa *elemento);
size_t pegar_indice(struct Vetor* vetor, struct Pessoa* pessoa, bool* deu_certo);
bool deletar_indice(struct Vetor* vetor, size_t indice, struct Pessoa *excluido);

#endif