#include "ui.h"
#include "pessoa.h"
#include "backend.h"
#include <string.h>
#include <stdio.h>
#include "vetor.h"
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>

enum OpcoesMenu {
    OPCAO_MENU_INVALIDA,
    OPCAO_ADICIONAR_PESSOA,
    OPCAO_PESQUISAR_PESSOA,
    OPCAO_ATUALIZAR_PESSOA,
    OPCAO_REMOVER_PESSOA,
    OPCAO_SAIR
};

struct DADOS_PESQUISA
{
    struct Vetor *resultado;
    char *pesquisa;
    bool porCpf;
};


bool inicializar_ui(void)
{
    return inicializar_backend();
}

void finalizar_ui(void)
{
    desligar_backend();
}


void limpar_tela(void)
{
    printf("\033[1;J");
    printf("\033[1;H");
    fflush(stdout);
}

void limpar_stdin(void)
{
    while (getchar() != '\n');
}

bool checar_fgets(char *str)
{
    for(char *c = str; *c != '\0'; c++) if (*c == '\n') return true;
    return false;
}

size_t indice_nova_linha(char *str) {
    size_t indice = 0;
    for(char *c = str; *c != '\0'; c++) 
        if (*c == '\n') 
        {
            indice = (size_t)c - (size_t)str;
            break;
        }
    return indice;
}

bool ler_atributo_string(char *atributo,const char* msg, size_t tamanho, bool tamanho_estrito)
{
    printf("%s", msg);
    if (fgets(atributo,tamanho,stdin) != atributo) return false;
    if (atributo[0] == '\n') return false;
    if (!checar_fgets(atributo))
    {
        limpar_stdin();
        return false;
    }
    if (tamanho_estrito  && indice_nova_linha(atributo) != tamanho - 2) return false;
    atributo[indice_nova_linha(atributo)] = '\n';
    return true;
}

bool str_e_num(char *s)
{
    for (char *c = s; *c != '\0'; c++) {
        if (!isdigit((int)*c)) return false;
    }
    return true;
}

char normalizar_sexo(char s)
{
    if (s == 'M')
        return 'm';
    else if (s == 'F')
        return 'f';
    else
        return s;
}

int menu(void)
{
    puts("==========MENU=========");
    printf("[%d] - para adicionar pessoas\n", OPCAO_ADICIONAR_PESSOA);
    printf("[%d] - para pesquisar uma pessoa\n", OPCAO_PESQUISAR_PESSOA);
    printf("[%d] - atualizar um pessoa\n", OPCAO_ATUALIZAR_PESSOA);
    printf("[%d] - para remover uma pessoa\n", OPCAO_REMOVER_PESSOA);
    printf("[%d] - para sair\n", OPCAO_SAIR);
    puts("=======================");
    printf("Digite sua opção: ");
    int op = 0;
    scanf("%d", &op);
    if (op < OPCAO_ADICIONAR_PESSOA || op > OPCAO_SAIR) op = OPCAO_MENU_INVALIDA;
    return op;
}

void opcao_invalida(void)
{
    limpar_stdin();
    limpar_tela();
    printf("Você digitou uma opção inválida! Pressione <ENTER> para voltar pro menu!");
    getchar();
}

bool adicionar_pessoas_frontend(void)
{
    limpar_stdin();
    enum DADOS_LIDOS
    {
        LEU_NOME = 1 << 0,
        LEU_CPF = 1 << 1,
        LEU_TELEFONE = 1 << 2,
        LEU_IDADE = 1 << 3,
        LEU_SEXO = 1 << 4,
    };

    struct Vetor *vetor = criar_vetor();

    if (vetor == NULL) return false;

    bool continuar = true;

    int dados_lidos = 0;

    struct Pessoa p = {0};
    do {
        if ((dados_lidos & LEU_NOME) == 0 && !ler_atributo_string(p.nome, "Nome: ", TAMANHO_NOME, false))
        {
            limpar_tela();
            printf("Você digitou um valor inválido para o campo 'Nome'.Aperte <ENTER> para tentar novamenete!");
            getchar();
            continue;
        }
        if(strstr(p.nome, "\n")) p.nome[indice_nova_linha(p.nome)] = '\0';
        dados_lidos |= LEU_NOME;
        if ((dados_lidos & LEU_CPF) == 0 && !ler_atributo_string(p.cpf, "Cpf: ", TAMANHO_CPF,true))
        {
            limpar_tela();
            printf("Você digitou um valor inválido para o campo  'Cpf'.Aperte <ENTER> para tentar novamenete!");
            getchar();
            continue;
        }
        dados_lidos |= LEU_CPF;
        if(strstr(p.cpf, "\n")) p.cpf[indice_nova_linha(p.cpf)] = '\0';
        if ((dados_lidos & LEU_TELEFONE) == 0 && !ler_atributo_string(p.telefone, "Telefone: ", TAMANHO_TEL, true))
        {
            limpar_tela();
            printf("Você digitou um valor inválido para o campo  'Telefone'. Aperte <ENTER> para tentar novamente!");
            getchar();
            continue;
        }
        dados_lidos |= LEU_TELEFONE;
        if(strstr(p.telefone, "\n")) p.telefone[indice_nova_linha(p.telefone)] = '\0';

        if ((dados_lidos & LEU_SEXO) == 0)
        {
            printf("Sexo[m/f]: ");
            scanf("%c", &p.sexo);

            p.sexo = normalizar_sexo(p.sexo);


            if (p.sexo != 'm' && p.sexo != 'f')
            {
                limpar_stdin();
                limpar_tela();
                printf("Você digitou um valor inválido para o campo 'Sexo'. Aperte <ENTER> para tentar novamente!");
                getchar();
                continue;
            }
        }

        dados_lidos |= LEU_SEXO;

        if ((dados_lidos & LEU_IDADE) == 0)
        {
            printf("Idade: ");
            unsigned idade_entrada = 0;
            scanf("%u", &idade_entrada);



            if (idade_entrada < 1 || idade_entrada > 255)
            {
                limpar_stdin();
                limpar_tela();
                printf("Você digitou um valor inválido para o campo 'Idade'. Aperte <ENTER> para tentar novamente!");
                getchar();
                continue;
            }

            p.idade = (unsigned char) idade_entrada;
        }
        assert(p.sexo == 'm' || p.sexo == 'f');

        if (!adicionar_elemento(vetor, &p)) return false;

        do {
            printf("Deseja continuar adicionando pessoas? [s/n]:  ");
            char resposta;
            limpar_stdin();
            scanf("%c", &resposta);
            limpar_stdin();
            resposta = (char)tolower((int)resposta);
            if (resposta != 's' && resposta != 'n')
            {
                limpar_tela();
                puts("Por favor digite apenas 's' ou 'n'!");
                continue;
            }
            if (resposta == 'n') continuar = false;
            dados_lidos = 0;
            break;
        } while(true);

    } while (continuar);

    int ret = adicionar_pessoas(vetor->buffer_pessoa, vetor->quantidade_elementos);

    switch (ret)
    {
        case OUTRO_ERRO:
            return false;
        case PESSOA_JA_CADASTRADA:
            limpar_tela();
            limpar_stdin();
            printf("Você está tentando adicionar uma pessoa com o mesmo cpf. Por favor aperte <ENTER> para tentar novamente!");
            getchar();
            adicionar_pessoas_frontend();
            break;
        default:
            break;
    }

    deletar_vetor(&vetor);
    return true;
}

void pesquisar_callcallback(struct Pessoa *atual, size_t index, void *dado)
{
    index = (size_t)index;
    struct DADOS_PESQUISA *dp = dado;
    bool adiciona = false;
    if (dp->porCpf)
    {
        adiciona = strstr(atual->cpf, dp->pesquisa) != NULL;
    }
    else
    {
        adiciona = strstr(atual->nome, dp->pesquisa) != NULL;
    }

    if (adiciona) adicionar_elemento(dp->resultado, atual);
}

void mostrar_tudo_callcallback(struct Pessoa *atual, size_t index, void *dado)
{
    dado = (void *)dado;
    printf("Código: %zu - Nome: %s - Cpf: %s - Telefone: %s - Sexo: %c - Idade: %d\n", index, atual->nome, atual->cpf, atual->telefone, atual->sexo, atual->idade);
}

void pesquisar_pessoa_frontend(void)
{
    limpar_stdin();
    limpar_tela();
    char pesquisa[TAMANHO_NOME];
    printf("Digite um cpf ou nome para pesquisar: ");
    fgets(pesquisa, TAMANHO_NOME, stdin);
    pesquisa[indice_nova_linha(pesquisa)] = '\0';
    struct DADOS_PESQUISA dp;
    dp.pesquisa = pesquisa;
    dp.porCpf = str_e_num(pesquisa);
    dp.resultado = criar_vetor();

    if (pega_quantidade_elementos() == 0) {
        limpar_tela();
        printf("Não foi adicionado nenhum registro. Aperte <ENTER> para voltar para o menu");
        getchar();
        return;
    }

    iterar(0, pega_quantidade_elementos() - 1, pesquisar_callcallback, (void *)&dp);

    if (dp.resultado->quantidade_elementos == 0)
    {
        limpar_tela();
        printf("Não houve resultados para sua busca.Aperte <ENTER> para voltar pro menu!");
        getchar();
        return;
    }

    limpar_tela();
    puts("Resultados Da Pesquisa: ");
    for (size_t i = 0; i < dp.resultado->quantidade_elementos; i++)
    {
        struct Pessoa *p = pegar_elemento_indice_ref(dp.resultado, i);
        printf("Nome: %s - Cpf: %s - Telefone: %s - Sexo: %c - Idade: %d\n", p->nome, p->cpf, p->telefone, p->sexo, p->idade);
    }
    printf("Pressione <ENTER> para voltar para o menu!");
    getchar();

    deletar_vetor(&dp.resultado);
}


void atualizer_pessoa_callback(struct Pessoa *atual, size_t index, void *dado)
{
    struct  {struct Vetor * vetor; size_t index;} *s = dado;

    if (s->index == index){
        adicionar_elemento(s->vetor, atual);
    }
}

void atualizar_pessoa_frontend(void)
{
    limpar_tela();
    limpar_stdin();
    if (pega_quantidade_elementos() == 0)
    {
        printf("Não tem registros para atualizar.Aperte <ENTER> para voltar pro menu!");
        getchar();
        return;
    }

    size_t codigo = 0;

    do {
        iterar(0, pega_quantidade_elementos() - 1, mostrar_tudo_callcallback, NULL);
        printf("Digite o código do registro que se deseja alterar: ");
        scanf("%zu", &codigo);
        limpar_stdin();
        if (codigo > pega_quantidade_elementos() - 1)
        {
            limpar_tela();
            printf("Você digitou um código inválido. Aperte <ENTER> para tentar novamente!");
            getchar();
            continue;
        }
        break;
    } while (true);

    struct Vetor *vetor = criar_vetor();

    iterar(0, pega_quantidade_elementos() - 1, atualizer_pessoa_callback, &(struct  {struct Vetor * vetor; size_t index;}){ vetor, codigo});

    size_t flags = NAO_ALTERA_NADA;
    struct Pessoa p ={0};
    do {
        bool resultado = ler_atributo_string(p.nome,"Digite o novo nome ou deixe em branco para não alterar: ", TAMANHO_NOME, false);
        if (!(strlen(p.nome) == 1 && p.nome[0] == '\n')){
            if (!resultado) {
                limpar_tela();
                printf("Você digitou um valor inválido.Aperte <Enter> para tentar novamente!");
                getchar();
                continue;
            }
        }
        if (strstr(p.nome, "\n")) p.nome[indice_nova_linha(p.nome)] = '\0';
        flags |= MUDAR_NOME;
        resultado = ler_atributo_string(p.cpf, "Digite o novo cpf ou deixe em branco para não alterar", TAMANHO_CPF, true);
        if (!(strlen(p.cpf) == 1 && p.cpf[0] == '\n')){
            if (!resultado) {
                limpar_tela();
                printf("Você digitou um valor inválido.Aperte <Enter> para tentar novamente!");
                getchar();
                continue;
            }
        }
        break;
    } while(true);
    deletar_vetor(&vetor);
}

void inicializar_loop_principal(void)
{
    bool sair = false;
    do {
        limpar_tela();
        int op = menu();
        switch (op)
        {
        case OPCAO_MENU_INVALIDA:
            opcao_invalida();
            break;
        case OPCAO_SAIR:
            sair = true;
            break;
        case OPCAO_ADICIONAR_PESSOA:
            adicionar_pessoas_frontend();
            break;
        case OPCAO_PESQUISAR_PESSOA:
            pesquisar_pessoa_frontend();
            break;
        case OPCAO_ATUALIZAR_PESSOA:
            atualizar_pessoa_frontend();
            break;
        default:
            break;
        }
    } while (!sair);
}
