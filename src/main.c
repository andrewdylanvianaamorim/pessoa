#include <stdio.h>
#include "ui.h"

int main(void)
{
    if (!inicializar_ui()) {
        fprintf(stderr, "Não foi possível inicializar o programa");
        return 1;
    }
    inicializar_loop_principal();
    finalizar_ui();
	return 0;
}
