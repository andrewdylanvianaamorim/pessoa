#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "../pessoa.h"
#include "../vetor.h"
#include "../backend.h"

void teste1(void)
{
    puts("Iniciando teste 1!");
    
    struct Vetor *v = NULL;
    if((v = criar_vetor()) == NULL) return;

    struct Pessoa p = {.nome = "Andrew Dylan", .cpf="11111111111", .telefone = "86994312527", .idade = 19, .sexo = 'M'};

    for (int i = 0; i < 50; i++) 
    {
        adicionar_elemento(v, &p);
    }
    

    assert(v->quantidade_elementos == 50);
    assert(v->capacidade == 64);

    struct Pessoa *pessoas = (struct Pessoa *)v->buffer_pessoa;
    for (int i = 0; i < 50; i++)
    {
        assert(strcmp(pessoas[i].nome, p.nome) == 0);
        assert(strcmp(pessoas[i].cpf, p.cpf) == 0);
        assert(strcmp(pessoas[i].telefone, p.telefone) == 0);
        assert(pessoas[i].idade == p.idade);
        assert(pessoas[i].sexo == p.sexo);
    }


    if (v != NULL) deletar_vetor(&v);
    
    assert(v == NULL);

    puts("Teste 1 finalizado com sucesso!");
}

void teste2(void)
{
    puts("Iniciando teste 2!");
    
    struct Vetor *v = NULL;
    if((v = criar_vetor()) == NULL) return;


    // lembrando que a comparação de pessoas é feita pelo cpf
    struct Pessoa pessoas[] = {
        {.nome = "Andrew", .cpf="11111111111", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Dylan", .cpf="22222222222", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Viana", .cpf="33333333333", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Amorim", .cpf="44444444444", .telefone = "86994312527", .idade = 19, .sexo = 'M'}
    };

    for (int i = 0; i < 3; i++) 
    {
        adicionar_elemento(v, &pessoas[i]);
        struct Pessoa *p = NULL;
        p = pegar_elemento_indice_ref(v, i);

        assert(strcmp(pessoas[i].nome, p->nome) == 0);
        assert(strcmp(pessoas[i].cpf, p->cpf) == 0);
        assert(strcmp(pessoas[i].telefone, p->telefone) == 0);
        assert(pessoas[i].idade == p->idade);
        assert(pessoas[i].sexo == p->sexo);
        
        bool deu_certo = false;
        size_t indice = pegar_indice(v, p, &deu_certo);
        assert(deu_certo);
        assert(indice == (size_t)i);
    }

    assert(pegar_elemento_indice_ref(v,3) == NULL);
    bool deu_certo = true;
    size_t indice = pegar_indice(v, &pessoas[3], &deu_certo);
    assert(!deu_certo);
    assert(indice == 0);


    if (v != NULL) deletar_vetor(&v);
    
    assert(v == NULL);

    puts("Teste 2 finalizado com sucesso!");
}

void teste3(void)
{
  puts("Iniciando teste 3!");
    
    struct Vetor *v = NULL;
    if((v = criar_vetor()) == NULL) return;


    // lembrando que a comparação de pessoas é feita pelo cpf
    struct Pessoa pessoas[] = {
        {.nome = "Andrew", .cpf="11111111111", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Dylan", .cpf="22222222222", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Viana", .cpf="33333333333", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Amorim", .cpf="44444444444", .telefone = "86994312527", .idade = 19, .sexo = 'M'}
    };

    for (int i = 0; i < 4; i++) 
    {
        adicionar_elemento(v, &pessoas[i]);
    }

    struct Pessoa excluido = {0};
    assert(deletar_indice(v,v->quantidade_elementos - 1, &excluido) == true);


    if (v != NULL) deletar_vetor(&v);
    
    assert(v == NULL);

    puts("Teste 3 finalizado com sucesso!");   
}

void teste4(void)
{
    puts("Iniciando teste 4!");

    struct Pessoa pessoas[] = {
        {.nome = "Andrew", .cpf="11111111111", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Dylan", .cpf="22222222222", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Viana", .cpf="33333333333", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Amorim", .cpf="44444444444", .telefone = "86994312527", .idade = 19, .sexo = 'M'}
    };

    struct Vetor *v = criar_vetor();
    if (v == NULL) return;

    int i = 0;
    for (i = 0; i < (int)(sizeof pessoas / sizeof(struct Pessoa)); i++)
    {
        adicionar_elemento(v, &pessoas[i]);
        struct Pessoa p = {0};
        assert(pegar_elemento_indice(v, (size_t)i, &p));
        assert(strcmp(p.nome, v->buffer_pessoa[i].nome) == 0);
        assert(strcmp(p.cpf, v->buffer_pessoa[i].cpf) == 0);
        assert(strcmp(p.telefone, v->buffer_pessoa[i].telefone) == 0);
        assert(p.idade == v->buffer_pessoa[i].idade);
        assert(p.sexo == v->buffer_pessoa[i].sexo);
    }


    struct Pessoa p = {0};
    assert(pegar_elemento_indice(v, (size_t)i, &p) == false);
    assert(pegar_elemento_indice(v, 0, NULL) == false);

    deletar_vetor(&v);
    puts("Teste 4 finalizado com sucesso!");
}

void clousure(struct Pessoa *atual, size_t index, void *data)
{
    struct Pessoa *pessoas = (struct Pessoa *)data;
    assert(compara_pessoas(atual, &pessoas[index]));
    assert(atual->sexo == pessoas[index].sexo);
}

void teste5(void)
{
    puts("Iniciando teste 5");
    
    assert(inicializar_backend());

    struct Pessoa pessoas[] = {
        {.nome = "Andrew", .cpf="11111111111", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Dylan", .cpf="22222222222", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Viana", .cpf="33333333333", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Amorim", .cpf="44444444444", .telefone = "86994312527", .idade = 19, .sexo = 'M'}
    }; 

    adicionar_pessoas(pessoas, 4);
    iterar(0, pega_quantidade_elementos() - 1, clousure, pessoas);

    assert(remover_pessoa(pessoas[3].cpf));

    struct Pessoa pessoas2[] = {
        {.nome = "Andrew", .cpf="11111111111", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Dylan", .cpf="22222222222", .telefone = "86994312527", .idade = 19, .sexo = 'M'},
        {.nome = "Viana", .cpf="33333333333", .telefone = "86994312527", .idade = 19, .sexo = 'M'}
    }; 

    iterar(0, pega_quantidade_elementos() - 1, clousure, pessoas2);

    assert(!remover_pessoa(pessoas[3].cpf));

    strcpy(pessoas2[0].nome, "ANDREW");
    pessoas2[0].idade = 20;
    atualizar_pessoa(pessoas2[0].cpf, &pessoas2[0], MUDAR_NOME | MUDAR_IDADE);
    
    iterar(0, pega_quantidade_elementos() - 1, clousure, pessoas2);
    desligar_backend();
    puts("Teste 5 finalizado com sucesso!");
}

int main(void)
{
    teste1();
    teste2();
    teste3();
    teste4();
    teste5();
    return 0;
}
