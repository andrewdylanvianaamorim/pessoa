#include "pessoa.h"
#include <string.h>


bool compara_pessoas(struct Pessoa* p1, struct Pessoa* p2)
{
	return strcmp(p1->cpf, p2->cpf) == 0;
}