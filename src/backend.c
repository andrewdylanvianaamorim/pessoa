#include "backend.h"
#include "pessoa.h"
#include "vetor.h"
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
static struct Vetor *VETOR = NULL;

bool inicializar_backend(void)
{
    return (VETOR = criar_vetor()) != NULL;
}

void desligar_backend(void)
{
    deletar_vetor(&VETOR);
}


int adicionar_pessoas(struct Pessoa pessoas[], size_t quantidade)
{
    for (size_t i = 0; i < quantidade; i++) {
        struct Pessoa *p = &pessoas[i];
        for (size_t i2 = 0; i2 < VETOR->quantidade_elementos; i2++)
        {
            if (compara_pessoas(p, &VETOR->buffer_pessoa[i2])) return PESSOA_JA_CADASTRADA;
        }
    }
    for (size_t i = 0; i < quantidade; i++)
    {
        if (!adicionar_elemento(VETOR, &pessoas[i])) return OUTRO_ERRO;
    }
    return SUCESSO;
}

bool remover_pessoa(const char *cpf)
{
    for (size_t i = 0; i < VETOR->quantidade_elementos; i++)
    {
        struct Pessoa p = {0};
        memcpy(p.cpf, cpf, sizeof(char) * TAMANHO_CPF);
        if (compara_pessoas(&p, &VETOR->buffer_pessoa[i]))
        {
            return deletar_indice(VETOR, i, NULL);
        }
    }
    return false;
}


bool atualizar_pessoa(char *cpf, const struct Pessoa *pessoa_nova, unsigned char flags)
{
    if (flags == NAO_ALTERA_NADA) return true;

    bool deu_certo = false;
    struct Pessoa p;
    memcpy(&p.cpf, &cpf, sizeof(char *));   
    size_t indice = pegar_indice(VETOR, &p, &deu_certo);
    if (!deu_certo) return false;

    struct Pessoa *pessoa_antiga = pegar_elemento_indice_ref(VETOR, indice);

    if ((flags & MUDAR_NOME) == MUDAR_NOME)
    {
        strcpy(pessoa_antiga->nome, pessoa_nova->nome);
    }

    if ((flags & MUDAR_CPF) == MUDAR_CPF)
    {
        for (size_t i = 0; i < VETOR->quantidade_elementos; i++)
            if (strcmp(VETOR->buffer_pessoa[i].cpf, pessoa_nova->cpf) == 0) return false;
        strcpy(pessoa_antiga->cpf, pessoa_nova->cpf);
    }

    if ((flags & MUDAR_TELEFONE) == MUDAR_TELEFONE)
    {
        strcpy(pessoa_antiga->telefone, pessoa_nova->telefone);
    }

    if ((flags & MUDAR_IDADE) == MUDAR_IDADE) pessoa_antiga->idade = pessoa_nova->idade;

    if ((flags & MUDAR_SEXO) == MUDAR_SEXO) pessoa_antiga->sexo = pessoa_nova->sexo;

    return true;
}

size_t pega_quantidade_elementos(void)
{
    return VETOR->quantidade_elementos;
}

void iterar(size_t comeco, size_t fim, void (func)(struct Pessoa *atual, size_t index, void *dado), void *dado)
{
    if (comeco > VETOR->quantidade_elementos - 1) return;
    if (fim < comeco || fim > VETOR->quantidade_elementos - 1) return;

    for (size_t index = comeco; index <= fim; index++)
    {
        func(pegar_elemento_indice_ref(VETOR,index), index, dado);
    }
}
